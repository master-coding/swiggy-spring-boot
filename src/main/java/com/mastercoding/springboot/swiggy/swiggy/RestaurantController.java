package com.mastercoding.springboot.swiggy.swiggy;

import com.mastercoding.springboot.swiggy.swiggy.beans.Restaurant;
import com.mastercoding.springboot.swiggy.swiggy.mapper.RestaurantMapper;
import com.mastercoding.springboot.swiggy.swiggy.service.RestaurantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.web.bind.annotation.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@RestController
public class RestaurantController {

    @Value("${swiggy.database.url}")
    String databaseUrl;

    @Value("${swiggy.database.driver}")
    String driverName;

    @Value("${swiggy.database.username}")
    String userName;

    @Value("${swiggy.database.password}")
    String password;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    RestaurantService restaurantService;


    @RequestMapping(value = "/restaurants1", method = RequestMethod.PUT)
    String addRestaurant(
            @RequestParam("name") String restName,
            @RequestParam("area") String area,
            @RequestParam("openTime") String openTime,
            @RequestParam("closeTime") String closeTime,
            @RequestParam("country") String country) {
        return restName + " : " + area + " : " + openTime + " : " + closeTime + " : " + country;
    }

    @RequestMapping(value = "/restaurants/{namePath}", method = RequestMethod.DELETE)
    ResponseEntity<Integer> deleteRestaurant(@PathVariable("namePath") String name) throws SQLException {
        Connection connection = Database.getConnection();
        String sql = "delete from swiggy.restaurant where name = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, name);
        int numRows = preparedStatement.executeUpdate();
        return new ResponseEntity<>(numRows, HttpStatus.OK);
    }

    @RequestMapping(value = "/restaurants", method = RequestMethod.GET)
    ResponseEntity<Restaurant> getRestaurant(@RequestParam("name") String name) throws ClassNotFoundException, SQLException {
        //get connection
        String url = "jdbc:mysql://127.0.0.1:3306/swiggy";
        Class.forName ("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection (url, "root", "");

        //create statement
        Statement statement = conn.createStatement();

        //execute query
        ResultSet result = statement.executeQuery("select * from restaurant where name = '" + name + "'");

        //accessing data
        Restaurant restaurant = new Restaurant();
        while (result.next()){
            String dbName = result.getString("name");
            String area = result.getString("area");
            Time openTime = result.getTime("open_time");
            Time closeTime = result.getTime("close_time");
            String country = result.getString("country");

            restaurant.setName(dbName);
            restaurant.setArea(area);
            restaurant.setOpenTime(openTime.toString());
            restaurant.setCloseTime(closeTime.toString());
            restaurant.setCountry(country);
        }
        return new ResponseEntity<>(restaurant, HttpStatus.OK);
    }

    @RequestMapping(value = "/restaurants2", method = RequestMethod.GET)
    ResponseEntity<List<Restaurant>> getRestaurantByArea(@RequestParam("area") String areaParam, @RequestParam("country") String countryParam) throws ClassNotFoundException, SQLException {
        Connection conn = null;
        PreparedStatement statement = null;
        List<Restaurant> restaurants = new ArrayList<>();

        try {
            //get connection
            String url = "jdbc:mysql://127.0.0.1:3306/swiggy";
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, "root", "");

            //create statement
            statement = conn.prepareStatement("select * from restaurant where area = ? and country = ?");
            statement.setString(1, areaParam);
            statement.setString(2, countryParam);

            //execute query
            ResultSet result = statement.executeQuery();

            //accessing data
            while (result.next()) {
                String dbName = result.getString("name");
                String area = result.getString("area");
                Time openTime = result.getTime("open_time");
                Time closeTime = result.getTime("close_time");
                String country = result.getString("country");

                Restaurant restaurant = new Restaurant();
                restaurant.setName(dbName);
                restaurant.setArea(area);
                restaurant.setOpenTime(openTime == null ? "" : openTime.toString());
                restaurant.setCloseTime(closeTime == null ? "" : closeTime.toString());
                restaurant.setCountry(country);
                restaurants.add(restaurant);
            }
        }finally {
            if(statement != null){
                statement.close();
            }
            if(conn != null){
                conn.close();
            }
        }
        return new ResponseEntity<>(restaurants, HttpStatus.OK);
    }

    @RequestMapping(value = "/restaurants1", method = RequestMethod.GET)
    ResponseEntity<List<Restaurant>> getRestaurants() throws ClassNotFoundException, SQLException {
        Connection conn = null;
        Statement statement = null;
        List<Restaurant> restaurants = new ArrayList<>();

        try {
            //get connection
            String url = "jdbc:mysql://127.0.0.1:3306/swiggy";
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, "root", "");

            //create statement
            statement = conn.createStatement();

            //execute query
            ResultSet result = statement.executeQuery("select * from restaurant order by area");

            //accessing data
            while (result.next()) {
                String dbName = result.getString("name");
                String area = result.getString("area");
                Time openTime = result.getTime("open_time");
                Time closeTime = result.getTime("close_time");
                String country = result.getString("country");

                Restaurant restaurant = new Restaurant();
                restaurant.setName(dbName);
                restaurant.setArea(area);
                restaurant.setOpenTime(openTime == null ? "" : openTime.toString());
                restaurant.setCloseTime(closeTime == null ? "" : closeTime.toString());
                restaurant.setCountry(country);
                restaurants.add(restaurant);
            }
        }finally {
            if(statement != null){
                statement.close();
            }
            if(conn != null){
                conn.close();
            }
        }
        return new ResponseEntity<>(restaurants, HttpStatus.OK);
    }

    @RequestMapping(value = "/restaurants", method = RequestMethod.PUT)
    ResponseEntity<Integer> addRestaurant(@RequestBody Restaurant restaurant) throws SQLException {
        int numRows = restaurantService.add(restaurant);
        return new ResponseEntity<>(numRows, HttpStatus.OK);
    }

    @RequestMapping(value = "restaurants/count", method = RequestMethod.GET)
    ResponseEntity<Integer> getRestaurantCount() throws ClassNotFoundException, SQLException {
        Class.forName(driverName);
        Connection conn = DriverManager.getConnection(databaseUrl, userName, password);
        PreparedStatement preparedStatement = conn.prepareStatement("select count(*) count from swiggy.restaurant");
        ResultSet resultSet = preparedStatement.executeQuery();
        int count = 0;
        if(resultSet.next()){
            count = resultSet.getInt("count");
        }

        return new ResponseEntity<>(count, HttpStatus.OK);
    }

    @RequestMapping(value = "restaurants/count1", method = RequestMethod.GET)
    ResponseEntity<Integer> getRestaurantCount1() throws ClassNotFoundException, SQLException {
        int count = jdbcTemplate.queryForObject(
                "SELECT COUNT(*) FROM swiggy.restaurant", Integer.class);
        return new ResponseEntity<>(count, HttpStatus.OK);
    }

    @RequestMapping(value = "restaurants/details", method = RequestMethod.GET)
    ResponseEntity<String> getRestaurantDetails() throws ClassNotFoundException, SQLException {
        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("name", "swagat")
                .addValue("area", "Miyapur");

        String area = namedParameterJdbcTemplate.queryForObject("SELECT area FROM swiggy.restaurant WHERE name = :name and area = :area", namedParameters, String.class);
        return new ResponseEntity<>(area, HttpStatus.OK);
    }

    @RequestMapping(value = "restaurants3", method = RequestMethod.GET)
    public ResponseEntity<List<Restaurant>> getRestaurants3() {
        String query = "SELECT * FROM swiggy.restaurant";
        List<Restaurant> restaurants = jdbcTemplate.query(
                query, new Object[]{}, new RestaurantMapper());
        return new ResponseEntity<>(restaurants, HttpStatus.OK);
    }

    @RequestMapping(value = "restaurants4", method = RequestMethod.GET)
    public ResponseEntity<List<Restaurant>> getRestaurants4() {
        List<Restaurant> restaurants = restaurantService.getRestaurants();
        return new ResponseEntity<>(restaurants, HttpStatus.OK);
    }
}
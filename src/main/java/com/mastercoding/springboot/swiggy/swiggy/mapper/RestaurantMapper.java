package com.mastercoding.springboot.swiggy.swiggy.mapper;

import com.mastercoding.springboot.swiggy.swiggy.beans.Restaurant;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RestaurantMapper implements RowMapper<Restaurant> {
    @Override
    public Restaurant mapRow(ResultSet rs, int rowNum) throws SQLException {
        String name = rs.getString("name");
        String country = rs.getString("country");
        String area = rs.getString("area");
        String openTime = rs.getTime("open_time") != null ? rs.getTime("open_time").toString() : "";
        String closeTime = rs.getTime("close_time") != null ? rs.getTime("close_time").toString() : "";

        Restaurant restaurant = new Restaurant();
        restaurant.setName(name);
        restaurant.setCountry(country);
        restaurant.setArea(area);
        restaurant.setOpenTime(openTime);
        restaurant.setCloseTime(closeTime);

        return restaurant;
    }
}
package com.mastercoding.springboot.swiggy.swiggy;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Database {
    private static String url = "jdbc:mysql://127.0.0.1:3306/swiggy";
    private static Connection connection = null;

    synchronized public static Connection getConnection() throws SQLException {
        if(connection != null){
            return connection;
        }
        connection = DriverManager.getConnection(url, "root", "");
        return connection;
    }
}

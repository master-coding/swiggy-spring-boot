package com.mastercoding.springboot.swiggy.swiggy.service;

import com.mastercoding.springboot.swiggy.swiggy.Database;
import com.mastercoding.springboot.swiggy.swiggy.beans.Restaurant;
import com.mastercoding.springboot.swiggy.swiggy.mapper.RestaurantMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Service
public class RestaurantService {
    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<Restaurant> getRestaurants() {
        String query = "SELECT * FROM swiggy.restaurant";
        List<Restaurant> restaurants = jdbcTemplate.query(
                query, new Object[]{}, new RestaurantMapper());
        return restaurants;
    }

    public int add(Restaurant restaurant) throws SQLException {
        Connection connection = Database.getConnection();
        PreparedStatement selectStatment = connection.prepareStatement("select * from swiggy.restaurant where name = ?");
        selectStatment.setString(1, restaurant.getName());
        ResultSet resultSet = selectStatment.executeQuery();
        int numRows = 0;
        if(resultSet.next()){
            //record already exists
            String updateQuery = "update swiggy.restaurant " +
                    " set name = ?, area = ?, open_time = ?, close_time = ?, country = ? " +
                    "where name = ?";
            PreparedStatement updateStatement = connection.prepareStatement(updateQuery);
            updateStatement.setString(1, restaurant.getName());
            updateStatement.setString(2, restaurant.getArea());
            updateStatement.setString(3, restaurant.getOpenTime());
            updateStatement.setString(4, restaurant.getCloseTime());
            updateStatement.setString(5, restaurant.getCountry());
            updateStatement.setString(6, restaurant.getName());

            numRows = updateStatement.executeUpdate();
        }else{
            //record not available
            String sql = "insert into swiggy.restaurant(name, area, open_time, close_time, country) values(?, ?, ?, ?, ?)";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, restaurant.getName());
            preparedStatement.setString(2, restaurant.getArea());
            preparedStatement.setString(3, restaurant.getOpenTime());
            preparedStatement.setString(4, restaurant.getCloseTime());
            preparedStatement.setString(5, restaurant.getCountry());

            numRows = preparedStatement.executeUpdate();
        }
        return numRows;
    }
}

package com.mastercoding.springboot.swiggy.swiggy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.mastercoding.test", "com.mastercoding.springboot.swiggy.swiggy"})
public class SwiggyApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext  applicationContext = SpringApplication.run(SwiggyApplication.class, args);
		Employee employee1 = applicationContext.getBean(Employee.class);
		employee1.setName("RAM");
		System.out.println("Employee 1: " + employee1.getName());

		Employee employee2 = applicationContext.getBean(Employee.class);
		System.out.println("Employee 2: " + employee2.getName());

//		Department department = applicationContext.getBean(Department.class);
//		department.print();

	}
}

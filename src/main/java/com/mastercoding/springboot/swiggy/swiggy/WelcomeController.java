package com.mastercoding.springboot.swiggy.swiggy;

import com.mastercoding.springboot.swiggy.swiggy.beans.Person;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class WelcomeController {
    @RequestMapping(value = "/welcome", method = RequestMethod.GET)
    String welcome(@RequestParam("name") String name,
                   @RequestParam("area") String area,
                   @RequestParam("aadharNumber") long aadharNumber,
                   @RequestParam("weight") float weight) {
        return "Welcome " + name + ", " + area + ", " + aadharNumber + ", " + weight;
    }

    @RequestMapping(value = "/welcome1", method = RequestMethod.GET)
    ResponseEntity<Person> welcome1(@RequestParam("name") String name,
                    @RequestParam("area") String area,
                    @RequestParam("aadharNumber") long aadharNumber,
                    @RequestParam("weight") float weight) {
        Person person = new Person();
        person.setName(name);
        person.setArea(area);
        person.setAadharNumber(aadharNumber);
        person.setWeight(weight);
        return new ResponseEntity<>(person, HttpStatus.OK);
    }
    @RequestMapping(value = "/welcome2", method = RequestMethod.POST)
    ResponseEntity<Person> welcome2(@RequestBody Person person) {
        return new ResponseEntity<>(person, HttpStatus.OK);
    }

}

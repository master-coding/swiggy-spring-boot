package com.mastercoding.springboot.swiggy.swiggy;

import org.springframework.stereotype.Component;

@Component
public class Department {
    private String name;

//    public Department(String name) {
//        this.name = name;
//    }

    public void print() {
        System.out.println("In Department : " + name);
    }
}

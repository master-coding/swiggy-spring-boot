package com.mastercoding.springboot.swiggy.swiggy;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CustomerController {
    @RequestMapping("/welcome/customer")
    String welcome() {
        return "Welcome to Swiggy customer";
    }

}

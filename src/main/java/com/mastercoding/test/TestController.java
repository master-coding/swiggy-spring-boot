package com.mastercoding.test;

import com.mastercoding.springboot.swiggy.swiggy.Department;
import com.mastercoding.springboot.swiggy.swiggy.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
    @Autowired
    Employee employee;

    @RequestMapping("/welcome/test")
    String welcome() {
        employee.print();
        return "Welcome to Swiggy customer";
    }
}
